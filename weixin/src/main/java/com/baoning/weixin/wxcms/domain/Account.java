package com.baoning.weixin.wxcms.domain;

import com.baoning.weixin.wxapis.process.MpAccount;
import lombok.Data;

import java.util.Date;

/**
 * Created By BaoNing On 2018/12/10
 */
@Data
public class Account extends MpAccount {

    /**
     * 名称
     */
    public String name;

    public Long id;

    public Date createTime = new Date();

}

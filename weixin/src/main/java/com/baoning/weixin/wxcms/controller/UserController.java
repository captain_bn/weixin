package com.baoning.weixin.wxcms.controller;

import com.baoning.weixin.wxcms.domain.User;
import com.baoning.weixin.wxcms.service.UserService;
import com.baoning.weixin.wxcms.vo.LoginVo;
import com.baoning.weixin.wxcore.util.AjaxResult;
import com.baoning.weixin.wxcore.util.MD5Util;
import com.baoning.weixin.wxcore.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created By BaoNing On 2018/12/7
 */
@RestController
@RequestMapping("api/v1/user")
public class UserController {

    @Resource
    protected HttpServletRequest request;

    @Resource
    protected HttpServletResponse response;

    @Autowired
    UserService userService;

    /**
     * 用户登录
     * @param loginVo
     * @return
     */
    @PostMapping("/login")
    public AjaxResult login(@RequestBody LoginVo loginVo){
        User user = loginVo.getUser();
        user.setPwd(MD5Util.getMD5Code(user.getPwd()));
        User sysUser = this.userService.getUser(user);
        if(sysUser == null ){
            return AjaxResult.failure("用户名或者密码错误!");
        }
        SessionUtil.setUser(sysUser);
        return AjaxResult.success(sysUser.getTrueName());
    }

    /**
     * 修改密码
     * @param loginVo
     * @return
     */
    @PostMapping("/updatepwd")
    public AjaxResult updatepwd(@RequestBody LoginVo loginVo){
        User user = loginVo.getUser();
        if(!SessionUtil.getUser().getPwd().equals(MD5Util.getMD5Code(user.getPwd()))){
            return AjaxResult.failure("用户名或者密码错误!");
        }
        user.setNewpwd(MD5Util.getMD5Code(user.getNewpwd()));
        this.userService.updateLoginPwd(user);
        //注销用户
        request.getSession().invalidate();
        return AjaxResult.success();
    }

    /**
     * 用户退出
     * @param request
     * @return
     */
    @GetMapping("/logout")
    public AjaxResult logout(HttpServletRequest request){
        request.getSession().invalidate();
        return AjaxResult.success();
    }


}

package com.baoning.weixin.wxcms.controller;

import com.baoning.weixin.wxcms.domain.Account;
import com.baoning.weixin.wxcms.service.AccountService;
import com.baoning.weixin.wxcore.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created By BaoNing On 2018/12/10
 */
@RestController
@RequestMapping("api/v1/account")
public class AccountController {

    @Autowired
    AccountService accountService;


    @GetMapping("/getById/{id}")
    public AjaxResult getById(@PathVariable("id") long id){
        Account account = accountService.getById(id);
        return AjaxResult.success(account);
    }

//    public AjaxResult listForPage(@RequestBody AccountVo accountVo){
//        Account account =  null;
//        List<Account> accounts = accountService.listForPage(accountVo);
//        if(CollectionUtils.isEmpty(accounts)){
//            return AjaxResult.success();
//        }
//        //aoount存入缓存中
//        if(null != accountVo && null != accountVo.getId()){
//            account = accountService.getById(accountVo.getId());
//            // TODO
//        }else {
//            account = accountService.getByAccount();
//        }
//        return AjaxResult.
//    }


}

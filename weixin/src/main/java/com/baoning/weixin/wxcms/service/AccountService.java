package com.baoning.weixin.wxcms.service;

import com.baoning.weixin.wxcms.domain.Account;
import com.baoning.weixin.wxcms.mapper.AccountMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created By BaoNing On 2018/12/10
 */
@Service
public class AccountService {

    @Autowired
    AccountMapper accountMapper;


    public Account getById(Long id){
        return accountMapper.getById(id);
    }

    public Account getByAccount(String account){
        return accountMapper.getByAccount(account);
    }

    public Account getSingleAccount(){
        return accountMapper.getSingleAccount();
    }

    public List<Account> listForPage(Account searchEntity){
        return accountMapper.listForPage(searchEntity);
    }

    public void add(Account entity){
        accountMapper.add(entity);
    }

    public void update(Account entity){
        accountMapper.update(entity);
    }

    public void delete(Account entity){
        accountMapper.delete(entity);
    }


}

package com.baoning.weixin.wxcms.vo;

import com.baoning.weixin.wxcms.domain.User;
import lombok.Data;

/**
 * Created By BaoNing On 2018/12/10
 */
@Data
public class UserVo extends User{
}

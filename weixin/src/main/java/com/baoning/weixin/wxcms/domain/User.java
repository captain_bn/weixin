package com.baoning.weixin.wxcms.domain;

import lombok.Data;

/**
 * Created By BaoNing On 2018/12/7
 */
@Data
public class User {

    /**
     * 主键id
     */
    private String id;

    /**
     * 用户名
     */
    private String account;

    /**
     * 密码
     */
    private String pwd;

    /**
     *性别 0男 1女
     */
    private String sex;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 真实姓名
     */
    private String trueName;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 更新时间
     */
    private String updateTime;

    /**
     * 状态
     */
    private String flag;

    /**
     * 新登录密码
     */
    private String newpwd;

}

package com.baoning.weixin.wxcms.mapper;

import com.baoning.weixin.wxcms.domain.Account;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Created By BaoNing On 2018/12/10
 */
@Mapper
public interface AccountMapper {

    Account getById(Long id);

    Account getByAccount(String account);

    Account getSingleAccount();

    List<Account> listForPage(Account searchEntity);

    void add(Account entity);

    void update(Account entity);

    void delete(Account entity);

}

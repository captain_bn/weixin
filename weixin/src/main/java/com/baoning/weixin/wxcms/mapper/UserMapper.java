package com.baoning.weixin.wxcms.mapper;

import com.baoning.weixin.wxcms.domain.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * Created By BaoNing On 2018/12/7
 */
@Mapper
public interface UserMapper {

    /**
     * 根据用户名密码查询
     * @param user
     * @return
     */
    User getUser(User user);

    /**
     * 根据主键id查询用户
     * @param userId
     * @return
     */
    User getUserById(@Param("userId") String userId);

    /**
     * 修改登录密码
     * @param user
     */
    int updateLoginPwd(User user);


}

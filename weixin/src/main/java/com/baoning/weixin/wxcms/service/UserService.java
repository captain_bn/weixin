package com.baoning.weixin.wxcms.service;


import com.baoning.weixin.wxcms.domain.User;
import com.baoning.weixin.wxcms.mapper.UserMapper;
import com.baoning.weixin.wxcore.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created By BaoNing On 2018/12/7
 */
@Service
public class UserService {

    @Autowired
    UserMapper userMapper;


    /**
     *获取用户
     * @param user
     * @return
     */
    public User getUser(User user){
        User resultUser = this.userMapper.getUser(user);
        if(resultUser != null){
            return resultUser;
        }else {
            return null;
        }
    }

    /**
     * 根据主键id查询用户
     * @param userId
     * @return
     */
    public User getUserById(String userId){
        User user = this.userMapper.getUserById(userId);
        if(user != null){
            return user;
        }else {
            return null;
        }
    }

    /**
     * 修改登录密码
     * @param user
     * @return
     */
    public int updateLoginPwd(User user){
        user.setUpdateTime(DateUtil.getNowTime());
        return this.userMapper.updateLoginPwd(user);
    }


}

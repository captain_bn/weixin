package com.baoning.weixin.wxcore.util;

import com.baoning.weixin.wxcms.domain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;
import java.util.HashMap;


/**
 * Created By BaoNing On 2018/12/10
 */
@Component
public final class SessionUtil {

    protected static final Logger logger = LoggerFactory.getLogger(SessionUtil.class);

    public static HttpSession session;

    @Autowired
    public void setSession(HttpSession session){
        this.session = session;
    }

    private static HashMap<String,HttpSession> sessions;
    private static HashMap<String,String> userSessionIds;

    public static final String SESSION_USER = "session_user";

    static {
        sessions = new HashMap<String, HttpSession>();
        userSessionIds = new HashMap<String, String>();
    }


    /**
     * 设置session值
     * @param key
     * @param value
     */
    public static void setAttr(String key, String value){
        session.setAttribute(key,value);
    }

    /**
     * 获取session值
     * @param key
     * @return
     */
    public static Object getAttr(String key){
        return session.getAttribute(key);
    }

    /**
     * 删除session值
     * @param key
     */
    public static void removeAttr(String key){
        session.removeAttribute(key);
    }

    /**
     * 设置用户信息到session
     * @param user
     */
    public synchronized static void setUser(User user){
        //踢出已有用户
        kickOutUserByUserAccount(user.getAccount());
        session.setAttribute(SESSION_USER,user);
        logger.debug("[session管理]用户有效,开始将session缓存起来,sessionid为: "+ session.getId() + ",用户登录账号:" + user.getAccount());
        userSessionIds.put(user.getAccount(),session.getId());
        addSession(session);
    }

    /**
     * 从session中获取用户信息
     * @return
     */
    public static User getUser(){
        return (User) session.getAttribute(SESSION_USER);
    }

    /**
     *从session中获取用户信息
     * @return
     */
    public static String getUserId(){
        User user = getUser();
        if(user != null){
            return user.getId();
        }
        return null;
    }

    /**
     * 根据用户账户踢出当前用户
     * @param account
     */
    public synchronized static void kickOutUserByUserAccount(String account){
        if(userSessionIds.get(account) != null){
            //找到此用户
            HttpSession session = sessions.get(userSessionIds.get(account));
            if(session != null){
                logger.debug("[session管理]踢出用户,sessionid为:" + session.getId() + ",用户登录账号: "+ account);
                removeSession(session);
                logger.debug("[session管理]踢出用户,将session设置为无效,sessionid为:" + session.getId() + "]");
                session.invalidate();
                sessions.remove(session.getId());
                //下线通知
                System.out.println("用户在线，踢出用户:" + session.getId());
                logger.debug("[session管理]踢出用户，开始发送下线通知，sessionid为：" + session.getId() + ",用户登陆账号：" + account);
            }
            userSessionIds.remove(account);
        }
    }

    /**
     * 将session加入到缓存中
     * @param session
     */
    public synchronized static void addSession(HttpSession session){
        logger.debug("[session管理]设置用户信息，将此session保存至内存，sessionid为：" + session.getId());
        sessions.put(session.getId(),session);
    }

    /**
     * 将session从缓存中删除
     * @param session
     */
    public synchronized static void removeSession(HttpSession session){
        logger.debug("[session管理]将session从内存中移除，sessionid为：" + session.getId());
        sessions.remove(session.getId());
    }


}

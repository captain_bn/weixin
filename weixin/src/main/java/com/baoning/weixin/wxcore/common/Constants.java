package com.baoning.weixin.wxcore.common;

/**
 * Created By BaoNing On 2018/12/10
 */
public class Constants {

/********************************* 操作信息常量 ***********************************/

    /**
     * 成功提示
     */
    public static final String MESSAGE_SUCCESS = "信息操作成功!";

    /**
     * 保存成功提示
     **/
    public static final String MESSAGE_SUCCESS_SAVE = "信息添加成功!";

    /**
     * 修改成功提示
     **/
    public static final String MESSAGE_SUCCESS_UPDATE = "信息修改成功!";

    /**
     * 删除成功提示
     **/
    public static final String MESSAGE_SUCCESS_DELETE = "信息删除成功!";


}

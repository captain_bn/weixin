package com.baoning.weixin.wxcore.util;


import com.baoning.weixin.wxcore.common.Constants;
import lombok.Data;


/**
 * Created By BaoNing On 2018/12/10
 */
@Data
public class AjaxResult {

    /**
     * 成功标识 true/false
     */
    public boolean success = false;

    /**
     * 提示信息
     */
    public String message;

    /**
     * 数据对象
     */
    public Object data = null;


/********************************* 操作成功 ***********************************/

    public static AjaxResult success(){
        AjaxResult result = new AjaxResult();
        result.setSuccess(true);
        result.setMessage(Constants.MESSAGE_SUCCESS);
        return result;
    }

    public static AjaxResult success(Object data){
        AjaxResult result = new AjaxResult();
        result.setSuccess(true);
        result.setMessage(Constants.MESSAGE_SUCCESS);
        result.setData(data);
        return result;
    }

    public static AjaxResult success(String message,Object data){
        AjaxResult result = new AjaxResult();
        result.setSuccess(true);
        result.setMessage(message);
        result.setData(data);
        return result;
    }


/********************************* 信息操作 ***********************************/

    /**
     * 信息保存成功
     * @return
     */
    public static AjaxResult saveSuccess(){
        AjaxResult result = new AjaxResult();
        result.setSuccess(true);
        result.setMessage(Constants.MESSAGE_SUCCESS_SAVE);
        return result;
    }

    /**
     * 信息修改成功
     * @return
     */
    public static AjaxResult updateSuccess(){
        AjaxResult result = new AjaxResult();
        result.setSuccess(true);
        result.setMessage(Constants.MESSAGE_SUCCESS_UPDATE);
        return result;
    }

    /**
     * 信息删除成功
     * @return
     */
    public static AjaxResult deleteSuccess(){
        AjaxResult result = new AjaxResult();
        result.setSuccess(true);
        result.setMessage(Constants.MESSAGE_SUCCESS_DELETE);
        return result;
    }

/********************************* 处理失败 ***********************************/

    public static AjaxResult failure(){
        AjaxResult result = new AjaxResult();
        result.setSuccess(false);
        return result;
    }

    public static AjaxResult failure(String message){
        AjaxResult result = new AjaxResult();
        result.setSuccess(false);
        result.setMessage(message);
        return result;
    }

    public static AjaxResult failure(String message, Object data){
        AjaxResult result = new AjaxResult();
        result.setSuccess(false);
        result.setMessage(message);
        result.setData(data);
        return result;
    }


}

package com.baoning.weixin.wxapis.process;

import lombok.Data;

/**
 * Created By BaoNing On 2018/12/10
 */

/**
 * 微信公众号信息
 */
@Data
public class MpAccount {

    /**
     * 账号
     */
    public String account;

    public String appid;

    public String appsecret;

    /**
     * 验证时的url
     */
    public String url;

    public String token;

    /**
     * 自动回复消息条数，默认是5
     */
    public Integer msgcount;

    public Integer getMsgcount() {
        if (msgcount == null)
            msgcount = 5;
            return msgcount;
    }

}
